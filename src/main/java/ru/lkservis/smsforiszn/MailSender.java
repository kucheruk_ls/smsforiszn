package ru.lkservis.smsforiszn;



import javax.mail.*;
import javax.mail.internet.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Я отправляю писмьо на емаил провайдера в рамках реализации SMTP API tele2
 *
 * @author leonid s kucheruk  shtormlbt@mail.ru
 * @version 1.0.0
 */
public class MailSender {

        private ConfigReader configReader;

        public MailSender(ConfigReader configReader){
            this.configReader = configReader;
        }

        /**
         *отправляю sms
         * @param map - структура данных содержащая номер телефона и одну или несколько sms которые на этот номер нужно
         *            отправить. Данную структуру готовит класс SmsCreator
         *
         */

        public void sendSms(HashMap<String, ArrayList<String>> map){
            if(map.size()>0) {
                //String to1 = configReader.getReceiverMail(); //адресат
                //String to2 = ConfigReader.getReceiverMail2(); //адресат 2
                String providerMailAddress = configReader.getProviderMailAddress();


                String from = configReader.getSenderMail();//отправитель
                String host = configReader.getSmtpServer();//smtp сервер
                String port = configReader.getSmtpPort();//порт smtp сервера
                String enablessl = configReader.getEnableSsl();//используется ли ssl
                String enableauth = configReader.getEnableAuth();//сервер использует автризацию?
                final String loginmail = configReader.getLoginSmtp();//логин к почте
                final String passmail = configReader.getPassSmtp();//пароль к почте
                String providerlogin = configReader.getProviderLogin();//логин провайдера для рассылки
                String providerpass = configReader.getProviderPsw();//пароль провайдера для рассылки
                String providersender = configReader.getProviderSender();//имя отправителя для sms

                Properties propertiesM = System.getProperties();
                propertiesM.setProperty("mail.smtp.host", host);
                propertiesM.setProperty("mail.smtp.port", port);
                propertiesM.setProperty("mail.smtp.ssl.enable", enablessl);
                propertiesM.setProperty("mail.smtp.auth", enableauth);


                Session session = Session.getDefaultInstance(propertiesM, new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(loginmail, passmail);
                    }
                });
                try {
                    MimeMessage message = new MimeMessage(session);//сообщение электронной почты
                    message.setFrom(new InternetAddress(from)); //настройка полей заголовка
                    message.addRecipients(Message.RecipientType.TO, providerMailAddress);
                    message.setSubject(configReader.getSubjectLine() + ""); //тема сообщения

                    Multipart multipart = new MimeMultipart();
                    MimeBodyPart textBodyPart = new MimeBodyPart();
                    //textBodyPart.
                    //textBodyPart.setText(configReader.getTextBodyPart()+"\n\n\n\n"+copyright);
                    StringBuilder sb = new StringBuilder();
                    for (Map.Entry<String, ArrayList<String>> entry : map.entrySet()) {
                        for (String str : entry.getValue()) {
                            //str = str.substring(1,str.length()-1);
                            ConsoleHandler.print("Будем отправлять: " + entry.getKey() + " " + str);
                            if (sb.length() == 0) {
                                sb.append(providerlogin + ":" + providerpass + ":0:0:0,0," + providersender + ":" + entry.getKey() + ":" + str);
                            } else {
                                sb.append("\n");
                                sb.append(providerlogin + ":" + providerpass + ":0:0:0,0," + providersender + ":" + entry.getKey() + ":" + str);
                            }
                            //textBodyPart.setText(providerlogin+":"+providerpass+":0:0:0,0,"+providersender+":"+entry.getKey()+":"+entry.getValue());

                        }
                    }

                    textBodyPart.setText(sb.toString(), "UTF-8");
                    multipart.addBodyPart(textBodyPart);

                    message.setContent(multipart);


                    //отправка сообщения
                    Transport.send(message);
                    ConsoleHandler.print(this.getClass().getSimpleName() + ": " + "Cообщение отправлено...");

                } catch (AddressException e) {
                    ConsoleHandler.print(this.getClass().getSimpleName() + ": " + "ошибка в адресе");
                    ConsoleHandler.printStackElements(e.getStackTrace());
                } catch (MessagingException e) {
                    ConsoleHandler.print(this.getClass().getSimpleName() + ": " + "ошибка в теле письма");
                    ConsoleHandler.printStackElements(e.getStackTrace());

                }
            }else{
                ConsoleHandler.print(this.getClass().getSimpleName()+": "+"нечего отправлять!");
            }
        }



}
