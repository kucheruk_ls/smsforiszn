package ru.lkservis.smsforiszn;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Моя задача, получив, от FileManager, список файлов извлеч из этих файлов смс и пометить файлы как обработанные.
 *
 * Класс контролирует что бы номер телефона состоял из 11 цифр - подряд, без пробелов и прочих символов и если номер начинается
 * на 8 она будет заменена на 7  - все! Остальные контроли на вашай совести. Ну или мыльте на shtormlbt@mail.ru - будем дорабатывать!
 *
 * @author leonid s kucheruk  shtormlbt@mail.ru
 * @version 1.0.0
 */
public class SmsCreator {
    ConfigReader configReader;

    public SmsCreator(ConfigReader configReader){
        this.configReader = configReader;

    }

    /**
     *
     * @param listPath - список файлов в директории обработки
     * @return - номера телефонов адресатов и sms сообщения которые нужно отправить
     */
    public HashMap<String, ArrayList<String>> smsHarvester(List<Path> listPath) {
        HashMap<String, ArrayList<String>> smsMap = new HashMap<>();

        for(Path pathF:listPath){
            String fileName = pathF.getFileName().toString();
            if(!fileName.startsWith(configReader.getPrefixSentTo())){
                File file = pathF.toFile();

                try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"WINDOWS-1251"))){
                    String readLine = "";
                    while ((readLine = bufferedReader.readLine())!=null){
                        //String line = new String(readLine.getBytes("WINDOWS-1251"),"UTF-8");
                        byte[] buffer = readLine.getBytes("WINDOWS-1251");
                        String line = new String(buffer,"WINDOWS-1251");

                        int idxProbel = line.indexOf("\t");
                        String phone = line.substring(0,idxProbel);
                        if(!phone.matches("(..........)[0-9]")){
                            ConsoleHandler.print(this.getClass().getSimpleName()+": "+"для телефона: "+phone+" не верный формат номера - отправка невозможна!");
                            break;
                        }
                        if(phone.startsWith("8")){
                            ConsoleHandler.print(this.getClass().getSimpleName()+": "+"для телефона: "+phone+" номер должен начинаться с 7, поменяем! ");
                            phone = phone.substring(1,phone.length());
                            phone = "7"+phone;
                        }

                        String tmpLine = line.substring(idxProbel,line.length()).trim();
                        String msg = tmpLine.trim();
                        if(msg.length()>140){
                            msg = msg.substring(0,140);
                        }

                        if(smsMap.containsKey(phone)){
                            ArrayList<String> tmpList = smsMap.get(phone);
                            tmpList.add(msg);
                            smsMap.put(phone,tmpList);
                        }else{
                            ArrayList<String> tmpList = new ArrayList<>();
                            tmpList.add(msg);
                            smsMap.put(phone,tmpList);
                        }


                    }

                    bufferedReader.close();




                }catch (FileNotFoundException e){
                    ConsoleHandler.print(this.getClass().getSimpleName()+": "+"файл "+file.getName()+" не найден!");
                }catch (UnsupportedEncodingException e){
                    ConsoleHandler.print(this.getClass().getSimpleName()+": "+"файл "+file.getName()+"имеет не поддерживаемую кодировку");
                }catch (IOException e){
                    ConsoleHandler.print(this.getClass().getSimpleName()+": "+"имеет место ошибка ввода вывода поэтому дальше выведу содержимое стека");
                    ConsoleHandler.printStackElements(e.getStackTrace());
                }






                Path newFileName = Paths.get(file.getParent()+"\\"+configReader.getPrefixSentTo()+file.getName());
                File newFile = newFileName.toFile();
                if(file.renameTo(newFile)){
                    ConsoleHandler.print(this.getClass().getSimpleName()+": "+"Файл "+file.toString()+" обработан и переименован успешно!");
                }else{
                    ConsoleHandler.print(this.getClass().getSimpleName()+": "+"Файл "+file.toString()+" не удалось переименовать после обработки!");
                }
                //Files.move(file.toPath(), newFileName, StandardCopyOption.REPLACE_EXISTING);

            }else{
                ConsoleHandler.print(this.getClass().getSimpleName()+": "+"Файл "+fileName+" уже помечен префиксом, обозначающим, что он уже был обработан");
            }
        }

        return smsMap;

    }


}
