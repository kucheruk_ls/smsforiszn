package ru.lkservis.smsforiszn;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 *
 * Я запускаю приложение.
 *
 * Данное приложение, забирает из указанной директории текстовые файлы, в кодировке WINDOWS-1251(хотя, если честно, с
 * другими не тестировалось но ожидаемый поток из файла в WINDOWS - 1251), содержащие номер телефона и, через
 * пробел, текст сообщения, и отправляет сообщения на указаный номер телефона. Сообщение обрезается до 140 симоволов.
 *
 * Интерфейс отправки реализует SMTP API от tele2
 *
 * @author leonid s kucheruk  shtormlbt@mail.ru
 *  * @version 1.0.0
 */
public class App {
    /**
     * да
     * @param args - метод не обрабатывает параметров
     */
    public static void main( String[] args ) {
        //ConfigReader configReader = new ConfigReader("config.xml");
        String fcfg = System.getProperty("user.dir")+"\\config.xml";
        ConfigReader configReader = new ConfigReader(fcfg);
        ConsoleHandler.print(configReader.getWorkDir());//тестовая печать
        Path workdir = Paths.get(configReader.getWorkDir());
        if(!workdir.toFile().isDirectory()){
            ConsoleHandler.print("main"+": "+"Ошбика чтения рабочей дирректории. Проверьте параметр workdir в файле config.xml, убедитесь что такая директория существует и доступна");
        }
        FileManager fileManager = null;
                try{
            fileManager = new FileManager(workdir);
                }catch (IOException e){
                    ConsoleHandler.print("main"+": "+"Ошбика чтения рабочей дирректории. Проверьте параметр workdir в файле config.xml");
                    ConsoleHandler.printStackElements(e.getStackTrace());
                }
        List<Path> files = fileManager.getFileList();
        
        for(Path s:files){
            ConsoleHandler.print(s.toString());
        }

        SmsCreator smsCreator = new SmsCreator(configReader);



        HashMap<String, ArrayList<String>> map=smsCreator.smsHarvester(files);

        MailSender mailSender = new MailSender(configReader);
        mailSender.sendSms(map);

    }
}
