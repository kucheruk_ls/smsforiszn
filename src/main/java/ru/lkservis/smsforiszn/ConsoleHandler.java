package ru.lkservis.smsforiszn;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Я пишу в консоль и лог! Файл smsforiszn.log находится в рабочей директории программ(там где *.jar). Если его там нет, он будет создан!
 * @author leonid s kucheruk  shtormlbt@mail.ru
 * @version 1.0.0
 */
public class ConsoleHandler {

    /**
     * метод пишет в консоль и лог строку полученную в качестве параметра - добавляя в переди дату получения строки
     * в виде 2018:12:16 11:13:22 | параметр string
     *
     * @param string - сообщение которое будет записано в лог и консоль
     */
    public static synchronized void print(String string){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
        String dt = sdf.format(date);

        Path logfile = Paths.get("smsforiszn.log");
        if(Files.notExists(logfile)){
            try{
                Files.createFile(logfile);
            }catch (IOException e){
                System.out.println("Ошибка создания лог файла");
            }
        }
        FileWriter fileWriter = null;
        try{
            fileWriter = new FileWriter(logfile.toFile(), true);
            try(BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
                bufferedWriter.append(dt+" | "+string+System.lineSeparator());

            }
        }catch (IOException e){
            System.out.println("В лог не могу писать((( совсем. Что то с ним не так!");
            e.printStackTrace();
        }

        System.out.println(dt+" | "+string);
    }

    /**
     * метод получает в качестве параметра массив элементов стека и построчно выводит их в лог и консоль
     *
     * @param elements - стэк
     */
    public static synchronized void printStackElements(StackTraceElement[] elements){

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY:MM:dd HH:mm:ss");
        String dt = sdf.format(date);

        Path logfile = Paths.get("smsforiszn.log");
        if(Files.notExists(logfile)){
            try{
                Files.createFile(logfile);
            }catch (IOException e){
                System.out.println("Ошибка создания лог файла");
            }
        }
        FileWriter fileWriter = null;
        try{
            fileWriter = new FileWriter(logfile.toFile(), true);
            try(BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){

                for(StackTraceElement el:elements){
                    bufferedWriter.append(dt+" | "+el.toString()+System.lineSeparator());
                }

            }
        }catch (IOException e){
            System.out.println("В лог не могу писать((( совсем. Что то с ним не так!");
            e.printStackTrace();
        }

        for(StackTraceElement el:elements){
            System.out.println(dt+" | "+el.toString());
        }


    }

}
