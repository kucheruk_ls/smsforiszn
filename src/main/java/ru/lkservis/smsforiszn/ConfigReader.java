package ru.lkservis.smsforiszn;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 * Я читаю конфиги.
 * Файл конфигурации должен находиться в одной папке с запускающим файлом smsforiszn-version-SNAPSHOT.jar и называться
 * config.xml.
 *
 *
 * Методы класса описывать не буду - все методы стандартные геттеры, т.е. читают конфиг. Класс не содержит методов для записи
 * в файл конфигурации.
 * @author leonid s kucheruk  shtormlbt@mail.ru
 * @version 1.0.0
 */
public class ConfigReader extends Properties {
    Path pathCfgFile;
    Properties properties;

    public ConfigReader(String pathCfgFile){
        File cfgfile = new File(pathCfgFile);
        this.pathCfgFile = cfgfile.toPath();
        this.properties = new Properties();
        try{
            properties.loadFromXML(new FileInputStream(cfgfile));
        }catch (FileNotFoundException e){
            ConsoleHandler.print( this.getClass().getName()+"Файл конфигурации не найден!");
        }catch (InvalidPropertiesFormatException e){
            ConsoleHandler.print(this.getClass().getSimpleName()+": "+"Файл конфигурации не найден!");
        }catch (IOException e){
            ConsoleHandler.print(this.getClass().getSimpleName()+": "+"Файл конфигурации не найден!");
        }
    }

    public ConfigReader(){
        this.pathCfgFile = Paths.get(System.getProperty("user.dir")+"\\config.xml");
    }

    /**
     *
     * @return директорию обработки
     */
    public String getWorkDir(){

        ConsoleHandler.print(this.getClass().getSimpleName()+": "+"директория обработки: "+properties.getProperty("workdir"));
        return properties.getProperty("workdir");
    }


    public String getPrefixSentTo(){
        ConsoleHandler.print(this.getClass().getSimpleName()+": "+"обработанные файлы будут помечаться префиксом: "+properties.getProperty("prefixsentto"));
        return properties.getProperty("prefixsentto");
    }

    public String getProviderMailAddress(){
        ConsoleHandler.print(this.getClass().getSimpleName()+": "+"email адрес провайдера: "+properties.getProperty("providermailaddress"));
        return properties.getProperty("providermailaddress");
    }

    public String getSenderMail(){
        return properties.getProperty("sendermail");
    }

    public String getSmtpServer(){
        return properties.getProperty("smtpserver");
    }

    public String getSmtpPort(){
        return properties.getProperty("smtpport");
    }

    public String getEnableSsl(){
        return properties.getProperty("enablessl");
    }

    public String getEnableAuth(){
        return properties.getProperty("enableauth");
    }

    public String getLoginSmtp(){
        return properties.getProperty("loginsmtp");
    }

    public String getPassSmtp(){
        return properties.getProperty("passsmtp");
    }

    public String getSubjectLine(){
        return properties.getProperty("subjectline");
    }

    public String getProviderLogin(){
        return properties.getProperty("providerlogin");
    }

    public String getProviderPsw(){
        return properties.getProperty("providerpsw");
    }

    public String getProviderSender(){
        return properties.getProperty("providersender");
    }
}
